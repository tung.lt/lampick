class tools {

  # package install list
  $packages = [
    "curl",
    "vim",
    "htop",
    "imagemagick",
    "libimage-exiftool-perl",
    "jnettop"
  ]

  # install packages
  package { $packages:
    ensure => present,
    require => Exec["apt-get update"]
  }
}
